FROM golang:1.19 as build-env

WORKDIR /go/src/app

COPY . .

RUN go get -d -v ./...
RUN go build -o request-monitoring-sidecar main.go

FROM gcr.io/distroless/base

COPY --from=build-env /go/src/app/request-monitoring-sidecar /go/src/app/request-monitoring-sidecar
ENV GIN_MODE release
WORKDIR /go/src/app

CMD ["./request-monitoring-sidecar"]

