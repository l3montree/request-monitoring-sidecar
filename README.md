# Request Monitoring sidecar

The request monitoring sidecar can be used to add influxdb monitoring to an application using the sidecar pattern.

## Usage
A configuration file `config.yaml` is expected inside the root of the project. The configuration file should look like this:

```yaml
proxy:
  port: 4000
  forwardTo: http://localhost:8080 # The URL to forward requests to
  pathRegex: ".*" # decides, whether this request should be MONITORED or not
influxdb:
  url: http://localhost:8086
  org: default
  bucket: monitoring
  measurement: request
```

AND the `INFLUXDB_TOKEN` environment variable should be set!

## Load Test Results

The proxy is able to handle 28439 requests with 100 concurrent users over the duration of 30 seconds. Each user has a fixed delay of 100ms. The mathematical limitation would be:
    
```math
Max = (1 / 0.1) * 30 * 100 = 30000
```

The proxy reaches an average request time of `105.62ms` in this scenario.

Measuring the target server without a proxy results in 29200 requests with 100 concurrent connections over a period of 30 seconds. The average requests time is `102.81ms`.

Thus the proxy adds an delay of `2.81ms` per request.