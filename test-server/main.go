package main

import (
	"net/http"
	"time"
)

func main() {
	// just build a simple http server which responds with a random status code.
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		time.Sleep(100 * time.Millisecond)
		w.WriteHeader(http.StatusOK)
		w.Write([]byte("hello world"))
	})
	http.HandleFunc("/api", func(w http.ResponseWriter, r *http.Request) {
		time.Sleep(200 * time.Millisecond)
		w.WriteHeader(http.StatusOK)
		w.Write([]byte("hello world - api"))
	})
	http.ListenAndServe(":8080", nil)
}
