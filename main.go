package main

import (
	"context"
	"fmt"
	"net/http"
	"net/http/httputil"
	"net/url"
	"regexp"
	"strings"
	"time"

	influxdb2 "github.com/influxdata/influxdb-client-go/v2"
	"github.com/influxdata/influxdb-client-go/v2/api"
	"github.com/spf13/viper"
)

func connectToInfluxDB(url, token, username, password, org, bucket string) (influxdb2.Client, error) {

	client := influxdb2.NewClient(url, token)
	if token == "" {
		client.Setup(context.Background(), username, password, org, bucket, 30*24)
	}

	// validate client connection health
	_, err := client.Health(context.Background())

	return client, err
}

type MonitoringRoundTripper struct {
	transport   http.RoundTripper
	writeApi    api.WriteAPI
	measurement string
	pathRegex   *regexp.Regexp
}

func (rt MonitoringRoundTripper) RoundTrip(r *http.Request) (resp *http.Response, err error) {
	// match the url regex
	if !rt.pathRegex.MatchString(r.URL.Path) {
		// if it does not match - just forward the request
		return rt.transport.RoundTrip(r)
	}
	// measure the start time
	start := time.Now()
	// do the real request.
	resp, err = rt.transport.RoundTrip(r)

	fields := make(map[string]interface{})

	fields["status"] = resp.StatusCode
	fields["duration"] = time.Since(start).Milliseconds()
	fields["method"] = resp.Request.Method
	fields["path"] = resp.Request.URL.Path

	queryValues := resp.Request.URL.Query()
	for key, value := range queryValues {
		if len(value) > 1 {
			fields[key] = strings.Join(value, ",")
		} else {
			fields[key] = value[0]
		}
	}

	point := influxdb2.NewPoint(rt.measurement, map[string]string{}, fields, time.Now())
	rt.writeApi.WritePoint(point)
	return resp, err
}

func NewMonitoringRoundTripper(writeApi api.WriteAPI, measurement string, urlRegex *regexp.Regexp) http.RoundTripper {
	return MonitoringRoundTripper{transport: http.DefaultTransport, writeApi: writeApi, measurement: measurement, pathRegex: urlRegex}
}

func main() {
	viper.SetConfigFile("config/config.yaml")

	err := viper.ReadInConfig()
	viper.AutomaticEnv()
	if err != nil {
		panic(err)
	}

	var username string
	var password string
	token := viper.GetString("INFLUXDB_TOKEN")
	if token == "" {
		// check if username and password is set.
		username := viper.GetString("INFLUXDB_USERNAME")
		password := viper.GetString("INFLUXDB_PASSWORD")
		if username == "" || password == "" {
			panic("INFLUXDB_TOKEN or INFLUXDB_USERNAME and INFLUXDB_PASSWORD must be set")
		}
	}

	influxURL := viper.GetString("influxdb.url")
	if influxURL == "" {
		panic("influxdb.url must be set")
	}

	bucket := viper.GetString("influxdb.bucket")
	if bucket == "" {
		panic("influxdb.bucket must be set")
	}

	org := viper.GetString("influxdb.org")
	if org == "" {
		panic("influxdb.org must be set")
	}

	client, err := connectToInfluxDB(influxURL, token, username, password, org, bucket)
	if err != nil {
		panic(err)
	}

	// get the forwardTo url
	forwardTo := viper.GetString("proxy.forwardTo")
	if forwardTo == "" {
		panic("proxy.forwardTo must be set")
	}

	target, err := url.Parse(forwardTo)
	if err != nil {
		panic(err)
	}
	proxy := httputil.NewSingleHostReverseProxy(target)

	measurement := viper.GetString("influxdb.measurement")
	if measurement == "" {
		measurement = "http_requests"
	}

	pathRegex := viper.GetString("proxy.pathRegex")
	if pathRegex == "" {
		pathRegex = ".*"
	}
	// build up the regexp
	regex, err := regexp.Compile(pathRegex)
	if err != nil {
		panic(err)
	}

	writeApi := client.WriteAPI(org, bucket)

	proxy.Transport = NewMonitoringRoundTripper(writeApi, measurement, regex)

	handler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		proxy.ServeHTTP(w, r)
	})

	port := viper.GetInt("proxy.port")

	http.ListenAndServe(fmt.Sprintf(":%d", port), handler)
}
